<?PHP           //por defecto le puse el tipo 0, despues habria que recibirlo como parametro
require "conexionBD.php";
$instruccion = "SELECT * FROM menu WHERE tipo_usuario = $tipoMenu ORDER BY secuencia";
$consulta = mysqli_query($conexion, $instruccion)
    or die("Fallo en la consulta");

?>

<nav class="navbar navbar-expand-sm navbar-light">
    <?php if ($tipoMenu = 1) { ?>
        <a class="navbar-brand" href="/practicoPhp/admin/index.php">
            <img src="logo.png" alt="logoParcial" height="60">
        </a>
    <?php } else { ?>
        <a class="navbar-brand" href="/practicoPhp/index.php">
            <img src="logo.png" alt="logoParcial" height="60">
        </a>
    <?php } ?>

    <ul class="navbar-nav">
        <?php
        for ($i = 0; $i < mysqli_num_rows($consulta); $i++) {
            $item = mysqli_fetch_array($consulta);
            print("<li class='nav-item'> <a class='nav-link' href=" . $item['link'] . ">" . $item["label"] . "</a></li>");
        }
        ?>
    </ul>
</nav>

<?php mysqli_close($conexion); ?>