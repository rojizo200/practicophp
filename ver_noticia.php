<!doctype html>
<html>
    <?php require "head.php";?>

    <body>
        <?php 
            require "conexionBD.php";
            $idNoticia=$_REQUEST['id'];
            $instruccion="SELECT * FROM noticias WHERE id = $idNoticia";
            $consulta=mysqli_query($conexion,$instruccion);
            
            $noticia=mysqli_fetch_array($consulta);
            $titulo=$noticia['titulo'];
            $cuerpo=$noticia['cuerpo'];
            $imagen=$noticia['img_destacada'];
            mysqli_close($conexion);
            ?>

        <?php 
            $modificar=false;
            $alta=false;
            $baja=false;
            session_start();
            if(isset($_SESSION['usuarioValido'])){
                if(isset($_SESSION['accion'])){
                    switch ($_SESSION['accion']) {
                        case 'modificar':$modificar=true;break;
                        case 'alta':$alta=true;break;
                        case 'baja':$baja=true;break;
                    }
                }
                $tipoMenu='1';
            }else{
                $tipoMenu='0';
            }
        ?>    
            
            <?php require "navbar.php"?>

        <div class="row">
            <div class=col-md-8>
            <div class="card">
                <div class="card-header">
                    <p class="card-title"><?php print($titulo);?></p>
                </div>
                <div class="card-body">
                    <img src=<?php print("imagenes/".$imagen);?> alt="FotoNoticia">
                </div>

                <div class=card-footer>
                    <p>
                        <?php print($cuerpo);?>
                    </p>
                    <?php if($modificar){
                        print("
                            <div class=card-footer>
                                <button type=button class=btn btn-outline-secondary>Modificar</button>
                            </div>
                        ");
                    } ?>
                </div>
            </div>
        </div>
</div>

    <?php require "footer.php"?>
</body>

</html>