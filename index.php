<!doctype html>
<html lang="en">

    <?php require "head.php";?>
    
    <!--funcion ajax para mas noticias-->
    <body onload=siguientePagina(0)>
        <?php
            session_start();
            if(isset($_SESSION['usuarioValido'])){
                $tipoMenu='1';
            }else{
                $tipoMenu='0';
            }
            if(isset($_GET['accion'])){
                switch ($_GET['accion']) {
                    case 'modificar':$_SESSION['accion']='modificar';$modificar=true;break;
                    case 'alta':$_SESSION['accion']='alta';$alta=true;break;
                    case 'baja':$_SESSION['accion']='baja';$baja=true;break;
                }
            }
            require "navbar.php";
        ?>

        <div id="divNoticias">

        </div>
        <button onclick="siguientePagina(this.value)" value=0 id="button">Mas noticias</button>
        <script>
            function siguientePagina(valor){
                document.getElementById("button").value=parseFloat(valor)+1;
                if(window.XMLHttpRequest){
                    xhttp=new XMLHttpRequest();
                }else if(window.ActiveXObject){
                    xhtto=new ActiveXObject("Microsoft.XMLHTTP");
                }else{
                    return "";
                }
                xhttp.onreadystatechange=function(){
                    if(this.readyState==4 && this.status==200){
                        document.getElementById("divNoticias").innerHTML+=this.responseText;
                    }
                }
                
                //aca dentro del getNoticias tengo que hacer la consulta a la base de datos
                xhttp.open("GET", "getNoticias.php?pagina="+valor, true);
                xhttp.send();
            }
        </script>

        <?php require "footer.php";?>

    </body>

</html>