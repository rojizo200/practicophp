<!--En este archivo muestro las noticias de 4 en 4-->

<?php 
    $administrador=false;
        $modificar=false;
        $alta=false;
        $baja=false;
        session_start();
        if(isset($_SESSION['usuarioValido'])){
            $administrador=true;
            if(isset($_SESSION['accion'])){
                switch ($_SESSION['accion']) {
                    case 'modificar':$modificar=true;break;
                    case 'alta':$alta=true;break;
                    case 'baja':$baja=true;break;
                }
            }
            $tipoMenu='1';
        }else{
            $tipoMenu='0';
        }
?>


<div class="row">
    <?php 
        header("Content-Type: text/html;charset=utf-8");
        require "conexionBD.php";
        if(isset($_REQUEST['pagina'])){
            $nroPagina=$_REQUEST['pagina'];
        }else{
            $nroPagina=0;
        }
        $minimo=$nroPagina*4;
        $maximo=$minimo+4;
        //ACA TENGO QUE HACER LA CONSULTA A LA BASE DE DATOS
        $instruccion="SELECT * FROM noticias LIMIT $minimo,$maximo";
        $consulta=mysqli_query($conexion,$instruccion);
            for($i=0;$i<mysqli_num_rows($consulta);$i++){
                $noticia=mysqli_fetch_array ($consulta);
                    print("
                        <div class=col-md-6>
                            <div class=row-md-6>
                                <div class=card>
                                    <div class=card-header>
                                        <a class=card-title href=ver_noticia.php?id=".$noticia['id']. ">" .$noticia['titulo']."</a>
                                    </div>
                                    <img class=card-body src=imagenes/" .$noticia['img_destacada']. " alt='FotoNoticia'>");
                                    if($administrador){
                                        print("
                                        <div class=card-footer>");
                                        if($modificar){
                                            print("<a href=/practicophp/admin/modificarNoticia.php?id=". $noticia['id']. ">Modificar</a>");
                                        }
                                        if($baja){
                                            print("<a href=/practicophp/admin/eliminarNoticia.php?id=". $noticia['id']. ">Eliminar</a>");
                                        }
                                        print("</div>");
                                    }
                                    print("</div>
                                </div>
                            </div>");
            }
                mysqli_close($conexion);
            ?>
</div>