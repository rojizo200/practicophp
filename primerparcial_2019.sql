-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 18-11-2019 a las 20:13:10
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `primerparcial_2019`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categorias`
--

CREATE TABLE `categorias` (
  `id` int(11) NOT NULL,
  `descripcion` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `categorias`
--

INSERT INTO `categorias` (`id`, `descripcion`) VALUES
(1, 'Policial'),
(2, 'Deportes'),
(3, 'Social'),
(4, 'Internacional');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `etiquetas`
--

CREATE TABLE `etiquetas` (
  `id` int(11) NOT NULL,
  `descripcion` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `etiquetas`
--

INSERT INTO `etiquetas` (`id`, `descripcion`) VALUES
(1, 'General Pico');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `menu`
--

CREATE TABLE `menu` (
  `idmenu` int(11) NOT NULL,
  `tipo_usuario` int(11) DEFAULT NULL,
  `label` text DEFAULT NULL,
  `link` text DEFAULT NULL,
  `secuencia` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `menu`
--

INSERT INTO `menu` (`idmenu`, `tipo_usuario`, `label`, `link`, `secuencia`) VALUES
(1, 0, 'Home', '/practicophp/index.php', 0),
(2, 0, 'Policial', '/practicophp/ver_categoria.php?id=1', 1),
(3, 0, 'Deportes', '/practicophp/ver_categoria.php?id=2', 3),
(4, 0, 'Social', '/practicophp/ver_categoria.php?id=3', 2),
(6, 1, 'Logout', '/practicophp/gestion/logout.php', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE `noticias` (
  `id` int(11) NOT NULL,
  `titulo` text DEFAULT NULL,
  `cuerpo` text DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `img_destacada` text DEFAULT NULL,
  `idcategorias` int(11) NOT NULL,
  `idautor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `titulo`, `cuerpo`, `fecha`, `img_destacada`, `idcategorias`, `idautor`) VALUES
(1, 'Continúan los trabajos de microaglomerado en la Ruta Provincial N° 7', 'Por el deterioro que generaron las inundaciones en su momento y el aumento del tránsito por los cortes de las rutas nacionales, se planificó una obra de conservación con una capa de microaglomerado en frío, evitando el ingreso del agua y que se sigan generando baches.\r\n\r\nLa obra a cargo de la empresa de Omar Ángle Jubete tiene un monto de contrato de $ 156.832.159,29 y un plazo de ejecución de 15 meses.\r\n\r\nLos trabajos se realizarán en 85 kilómetros, desde la Ruta Provincial Nº 102 en proximidades de Metileo, hasta la intersección con la Ruta Nacional Nº 5, en cercanías de Anguil.\r\n\r\nComprende todos los tramos y se busca rectificar la rasante en aquellos lugares donde haya ondulaciones, previo saneamiento con bacheo y la capa de micro en frío, con toda la señalización vertical y horizontal.', '2019-10-01', 'img1.jpg', 3, 1),
(2, '«Te voy a prender fuego, te voy a cagar degollando»: Lo condenaron a un año y medio por amenazar a su ex con un cuchillo de carnicero y golpear a un policía', 'Alberto Felipe Eberts, un lavacoches de 30 años, fue condenado hoy a un año y seis meses de prisión de cumplimiento efectivo, como autor de los delitos de amenazas calificadas por el uso de arma –enmarcado en la ley 26485 de Protección Integral a las Mujeres–, lesiones leves y lesiones leves calificadas por ser contra personal policial, en concurso ideal con resistencia a la autoridad, concursando a su vez dichos delitos materialmente entre sí. Además fue declarado reincidente.', '2019-10-01', 'img2.jpg', 1, 1),
(3, 'Anunciaron las «Primeras Jornadas Pampeanas de Cáncer de Colorrectal»', 'Las jornadas fueron anunciadas en conferencia de prensa realizada esta mañana en el salón auditorio del hospital Gobernador Centeno de General Pico\r\n\r\nLas mismas son organizadas por la Dirección de APS y la Dirección de Endoscopía del Hospital Gobernador Centeno en las cuales se contará con la participación de destacados profesionales del área de la salud de la provincia.\r\n\r\nLas Primeras Jornadas Pampeanas de Cáncer de Colorrectal están destinadas a todo el equipo de salud de la ciudad, tanto del ámbito público como privado.', '2019-10-01', 'img3.jpeg', 3, 1),
(4, 'La UNLPam entregará medallas de reconocimiento por 25 años de servicio a personal docente y no docente', 'SEDE GENERAL PICO\r\nDurante el acto de la sede General Pico recibirán medalla las docentes de la Facultad de Ciencias Humanas Perla Viviana Garayo y María Rosana Moretta, junto a las y los docentes de la Facultad de Ciencias Veterinarias María de los Ángeles Bruni; Valeria Graciela Buey; Aldo Daniel Manso; Guillermo Esteban Meglia y Enrique Eberardo Schmidt.\r\n\r\nDe la Facultad de Ingeniería recibirán las y los docentes Carlos Alberto Ballesteros; Gabriela Fabiana Minetti y Estela Raquel Ramos.\r\nCon respecto a personal nodocente, de la sede General Pico de Rectorado recibirá medalla Graciela Alvarez.', '2019-10-01', 'img4.jpg', 3, 1),
(5, 'BOMBEROS: Realizarán un encuentro provincial de cadetes en General Pico', 'El sábado 2 y domingo 3 se llevará a cabo una nueva edición del Encuentro Provincial de Cadetes en las instalaciones de El Campito, predio ubicado en General Pico.\r\n\r\nMás de 60 cadetes y cadetas recibirán capacitación de parte de los Deptos. de Protocolo y Ceremonial, Seguridad Bomberil, BREC y de Comando e Incidentes.\r\n\r\nLos responsables del desarrollo del Encuentro serán los integrantes del Depto. Provincial de Cadetes y la logística estará a cargo de la Asociación de BBVV de General Pico.', '2019-10-01', 'img5.jpg', 3, 1),
(6, 'Críticas de CTERA a los lineamientos generales del programa “Secundaria 2030”', 'El pasado día 23 de octubre se reunieron en CTERA Secretarios de Educación Secundaria de AGMER, AMSAFE, ATEN, APEM, SUTECO, SUTEBA, UTE, UTRE UNTER y UTELPa, para analizar la situación del nivel secundario en el contexto actual.\r\n\r\nLos referentes de educación de los sindicatos de base de CTERA aunaron sus críticas a los lineamientos generales del Programa “Secundaria 2030”, al considerar que desdibuja la responsabilidad del Estado, abandona la concepción de derecho y enfatiza la de servicio, somete la formación secundaria a los requerimientos del mercado y el empleo, deslegitima el trabajo docente y lo reduce al rol de facilitador, y focaliza en la concepción instrumental del conocimiento.', '2019-10-01', 'img6.jpg', 3, 1),
(7, 'Mayoral repartió bananas respecto al viralizado Twitt de la diputada Adriana Leher en la que trató de monos a los votantes del Fente de Todos', 'Ocurrió en la Cámara Legislativa provincial. En una jornada donde lo más saliente fue la ocurrencia de la diputada Alicia Mayoral de repartir bananas a cuanto diputado estaba presente aguardando el inicio de la comisión de legislación social y salud pública, ironizando respecto al viralizado twit de la diputada Adriana Leher –la primera a la que le entregó la fruta- en la que supuestamente trata de monos a los votantes del Fente de Todos, los legisladores que tuvieron actividad hoy, fueron los que integran la comisión señalada y los de hacienda y presupuesto y asuntos agrarios, turismo, industria y obras públicas que se reunieron en plenario.', '2019-10-01', 'img7.jpg', 3, 1),
(8, 'La artista plástica María José Pérez expone en Casa de La Pampa', 'En una nueva edición del ciclo de Artes Visuales 2019, la Casa de La Pampa inaugura la muestra “El horizonte y el cuerpo”, de la artista plástica María José Pérez, con la curaduría de Fabiana Barreda.\r\n\r\nEl evento tendrá lugar el miércoles 6 de noviembre a las 20 horas en la sede de la representación provincial, ubicada en Suipacha 346, CABA, con entrada libre y gratuita. El cierre musical estará a cargo del pampeano Jableach (Javier Oros).   ', '2019-10-01', 'img8.jpg', 3, 1),
(9, 'El Senado de la Nación le entregará su máximo galardón a Rubén Hugo Marín', 'El próximo Martes 5 de noviembre, el Senado de la Nación Argentina entregará la mención de honor «Senador Domingo Faustino Sarmiento» al político pampeano, ex gobernador, senador nacional (M.C) y presidente del Partido Justicialista de la provincia de La Pampa, Dr. Rubén Hugo Marín.\r\n\r\nLa ceremonia de entrega será a las 16 horas en el Salón de los Pasos Perdidos del palacio legislativo nacional.', '2019-10-01', 'img9.jpg', 3, 1),
(10, 'Cobran los estatales pampeanos\r\n', 'El próximo Martes 5 de noviembre, el Senado de la Nación Argentina entregará la mención de honor «Senador Domingo Faustino Sarmiento» al político pampeano, ex gobernador, senador nacional (M.C) y presidente del Partido Justicialista de la provincia de La Pampa, Dr. Rubén Hugo Marín.\r\n\r\nLa ceremonia de entrega será a las 16 horas en el Salón de los Pasos Perdidos del palacio legislativo nacional.', '2019-10-01', 'img10.jpg', 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias_etiquetas`
--

CREATE TABLE `noticias_etiquetas` (
  `idnoticias` int(11) NOT NULL,
  `idetiquetas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `noticias_etiquetas`
--

INSERT INTO `noticias_etiquetas` (`idnoticias`, `idetiquetas`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `img_perfil` text DEFAULT NULL,
  `descripcion` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `username`, `password`, `email`, `tipo`, `img_perfil`, `descripcion`) VALUES
(1, 'admin', 'admin', 'admin@admin', 1, 'admin.jpg', 'admin');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categorias`
--
ALTER TABLE `categorias`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `etiquetas`
--
ALTER TABLE `etiquetas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`idmenu`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_noticias_categorias_idx` (`idcategorias`),
  ADD KEY `fk_noticias_usuarios1_idx` (`idautor`);

--
-- Indices de la tabla `noticias_etiquetas`
--
ALTER TABLE `noticias_etiquetas`
  ADD PRIMARY KEY (`idnoticias`,`idetiquetas`),
  ADD KEY `fk_noticias_has_etiquetas_etiquetas1_idx` (`idetiquetas`),
  ADD KEY `fk_noticias_has_etiquetas_noticias1_idx` (`idnoticias`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `noticias`
--
ALTER TABLE `noticias`
  ADD CONSTRAINT `fk_noticias_categorias` FOREIGN KEY (`idcategorias`) REFERENCES `categorias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_noticias_usuarios1` FOREIGN KEY (`idautor`) REFERENCES `usuarios` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `noticias_etiquetas`
--
ALTER TABLE `noticias_etiquetas`
  ADD CONSTRAINT `fk_noticias_has_etiquetas_etiquetas1` FOREIGN KEY (`idetiquetas`) REFERENCES `etiquetas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_noticias_has_etiquetas_noticias1` FOREIGN KEY (`idnoticias`) REFERENCES `noticias` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
