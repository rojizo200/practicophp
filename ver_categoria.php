<!doctype html>
<html>
    <?php require "head.php";?>

    <body>
        <?php 
            $modificar=false;
            $alta=false;
            $baja=false;
            session_start();
            if(isset($_SESSION['usuarioValido'])){
                if(isset($_SESSION['accion'])){
                    switch ($_SESSION['accion']) {
                        case 'modificar':$modificar=true;break;
                        case 'alta':$alta=true;break;
                        case 'baja':$baja=true;break;
                    }
                }
                $tipoMenu='1';
            }else{
                $tipoMenu='0';
            }
        ?>
        
        <?php require "navbar.php"?>


        <?php 
            require "conexionBD.php";
            $idCategoria=$_REQUEST['id'];
            $instruccion="SELECT * FROM noticias WHERE idcategorias = $idCategoria";
            $consulta=mysqli_query($conexion,$instruccion);
            for($i=0;$i<mysqli_num_rows($consulta);$i++){
                $noticia=mysqli_fetch_array ($consulta);
                print("
                    <div class=col-md-6>
                        <div class=row-md-6>
                            <div class=card>
                                <div class=card-header>
                                    <a class=card-title href=ver_noticia.php?id=".$noticia['id']. ">" .$noticia['titulo']."</a>
                                </div>
                                <img class=card-body src=imagenes/" .$noticia['img_destacada']. " alt='FotoNoticia'>");
                                if($modificar){
                                    print("
                                        <div class=card-footer>
                                            <button type=button class=btn btn-outline-secondary>Modificar</button>
                                        </div>
                                    ");
                                }
                        
                           print(" </div>
                        </div>
                    </div>"
                );
            }

            mysqli_close($conexion);
        ?>
        <?php require "footer.php"?>
    </body>

</html>