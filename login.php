<!doctype html>
<html>
<?php require "head.php";?>

<body>
    <?php require "conexionBD.php";?>


    <!-- ACA ME TENGO QUE FIJAR SI ES LA PRIMERA VEZ O SI PUSO ALGO MAL-->
    <?php 
        $error=false;
        session_start();
        if(isset($_POST["username"]) && isset($_POST["pass"])){
            $usuario=$_POST["username"];
            $pass=$_POST["pass"];
            $instruccion="SELECT * FROM usuarios WHERE username = '$usuario' AND password = '$pass'";
            $consulta=mysqli_query($conexion,$instruccion) or die("Error en la consulta");
            if(mysqli_num_rows($consulta)>0){
                $usuarioValido=$usuario;
                $_SESSION["usuarioValido"]=$usuario;
                header("Location: /practicophp/admin/index.php", true, 301);

                //ACA LOS TENGO QUE REDIRIGIR AL ADMIN/INDEX.PHP
            }else{
                //EL USUARIO O LA CONTRASEÑA CON INCORRECTOS
                $error=true;
            }
        }
        mysqli_close($conexion);
    ?>
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Iniciar Sesion!</h1>
            <p class="lead">
                <?php if($error){
                    print("<div class=alert alert-danger role=alert>");
                        print("<p>Usuario o contraseña incorrectos</p>");
                    print("</div>");
                } ?>
                

                <form action='' method='post'>
                    <div class="form-group">
                        <label for="username">Email</label>
                        <input type="text" class="form-control" id="username" name="username" placeholder="Enter email">

                    </div>
                    <div class="form-group">
                        <label for="pass">Password</label>
                        <input type="password" class="form-control" id="pass" name="pass" placeholder="Password">
                    </div>
                    <input type="submit" class="btn btn-primary" value="Login">
                </form>
            </p>
        </div>
    </div>
    <?php include "footer.php";?>
</body>

</html>