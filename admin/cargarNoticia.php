<!doctype html>
<html>
<?php require "../head.php" ?>

<?php
require "../conexionBD.php";
$recuperarid =mysqli_query($conexion, "SELECT * \n". "FROM noticias \n". "WHERE id=(\n". "    SELECT max(id) FROM noticias\n". "    )");

$row = mysqli_fetch_array($recuperarid);
$pidmax=$row['id'];

?>

<?php
        session_start();
        if(! isset($_SESSION['usuarioValido'])){
            header("Location: /practicoPhp/index.php", true, 301);
        }
    ?>

<?php
//SUBIENDO FORMULARIO
if (isset($_POST['enviar'])) {
    $error = false;
    $copiarFichero = false;
    $directorio = '../imagenes/';
    if (is_uploaded_file($_FILES['imagen']['tmp_name'])) {
        $idUnico = time();
        $nombreFichero = $idUnico . "-" . $_FILES['imagen']['name'];
        $copiarFichero = true;
    } else {
        $error = true;
        print("<h1>Error 1</h1>");
    }

    if (!$error) {
        $pidmax=$pidmax+1;
        $fecha= date("Y-m-d");
        $instruccion = "INSERT INTO `noticias` (`id`, `titulo`, `cuerpo`, `fecha`, `img_destacada`, `idcategorias`, `idautor`) 
        VALUES ('$pidmax','$_POST[titulo]',' $_POST[cuerpo]', '$fecha' ,'$nombreFichero',' $_POST[categoria]','$_POST[autor]')";
        $consulta = mysqli_query($conexion, $instruccion) or die("Fallo en la insersion");
        mysqli_close($conexion);
    }
    

    if ($copiarFichero){
        move_uploaded_file ($_FILES['imagen']['tmp_name'],$directorio . $nombreFichero);
    }
}


?>

<body>
    <?php $tipoMenu = '1';
    require "../navbar.php"; ?>
    <form action="cargarNoticia.php" method="POST" id="formulario" enctype="multipart/form-data">
        <div class=form-group>
            <input type=number class=form-control name="categoria" id="categoria" cols="80" form="formulario" placeholder="Ingrese la categoria de la nueva noticia" @required></textarea>
        </div>
        <div class=form-group>
            <textarea class=form-control name="titulo" id="titulo" cols="50" rows="2" form="formulario" placeholder="Ingrese el Titulo de la nueva noticia" @required></textarea>
        </div>
        <input class="form-control" type="file" name="imagen" id="imagen">
        <div class=form-group>
            <textarea class=form-control name="cuerpo" id="cuerpo" cols="80" rows="10" form="formulario" placeholder="Ingrese el cuerpo de la nueva noticia" @required></textarea>
        </div>
        <div class=form-group>
            <input type=number class=form-control name="autor" id="autor" cols="80" form="formulario" placeholder="Ingrese el Autor de la nueva noticia" @required></textarea>
        </div>
        <input type="submit" name="enviar" value="Confirmar">

    </form>

    <?php require "../footer.php" ?>
</body>

</html>