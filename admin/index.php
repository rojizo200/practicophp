<!doctype html>
    <html>
    <?php require "../head.php"?>

    <!-- TENGO QUE CHEQUEAR EL MANEJO DE SESIONES-->
    <?php 
        session_start();
        if($_SESSION["usuarioValido"]==null){
            header("Location: /practicophp/index.php", true, 301);
        }
    ?>

    <body>
        <?php $tipoMenu=1; require "../navbar.php"?>
        <div>
            <h1>
                Bienvenido admin!
            </h1>
        </div>

        <div>
            <h3>
                Acciones:
            </h3>
        </div>

        <div class="row">
            <a href="../admin/cargarNoticia.php">
                <img src="../imagenes/add.png" alt="OpcionMenu" height="150px">
            </a>
            <a href="/practicophp/index.php?accion=modificar">
                <img src="../imagenes/edit.png" alt="OpcionMenu" height="150px">
            </a>
            <a href="/practicophp/index.php?accion=baja">
                <img src="../imagenes/remove.png" alt="OpcionMenu" height="150px">
            </a>
        </div>

        <?php require "../footer.php"?>
    </body>

    </html>