<!doctype html>
<html>
    <?php require "../head.php" ?>

    <?php 
        require "../conexionBD.php";
        $idNoticia=$_REQUEST['id'];
        $instruccion="SELECT * FROM noticias WHERE id = $idNoticia";
        $consulta=mysqli_query($conexion,$instruccion);
        
        $noticia=mysqli_fetch_array($consulta);
        $titulo=$noticia['titulo'];
        $cuerpo=$noticia['cuerpo'];
        $imagen=$noticia['img_destacada'];
    ?>

    <?php
        session_start();
        if(! isset($_SESSION['usuarioValido'])){
            header("Location: /practicoPhp/index.php", true, 301);
        }
    ?>

    <?php 
    //SUBIENDO FORMULARIO
        if(isset($_POST['enviar'])){
            $error=false;
            $copiarFichero=false;
            $directorio='../imagenes/';
            if(is_uploaded_file($_FILES['imagen']['tmp_name'])){
                $idUnico=time();
                $nombreFichero= $idUnico . "-" . $_FILES['imagen']['name'];
                $copiarFichero=true;
            }else{
                $error=true;
                print("<h1>Error 1</h1>");
            }
            //LO IDEAL SERIA VERIFICAR QUE NO ESTEN VACIOS, LO HAGO CON HTML, PERO NO CREO QUE SEA LO CORRECTO
            if(! $error){
                $titulo=$_POST['titulo'];
                $cuerpo=$_POST['cuerpo'];
                $instruccion="UPDATE `noticias` SET `titulo`='$titulo',`cuerpo`='$cuerpo',`img_destacada`='$nombreFichero' WHERE id=$idNoticia";
                $consulta=mysqli_query($conexion,$instruccion) or die("Fallo en la consulta");
                mysqli_close($conexion);
            }

            if ($copiarFichero){
                move_uploaded_file ($_FILES['imagen']['tmp_name'],$directorio . $nombreFichero);
            }

        }
    
    
    ?>
    <body>
        <?php $tipoMenu='1'; require "../navbar.php";?>
        <form action="" method="POST" id="formulario" enctype="multipart/form-data">
            <div class=form-group>
                <textarea class=form-control name="titulo" id="titulo" cols="50" rows="5" form="formulario" required><?php print($titulo);?></textarea>
            </div>
            <input class="form-control" type="file" name="imagen" id="imagen" required>
            <div class=form-group>
                <textarea class=form-control name="cuerpo" id="cuerpo" cols="80" rows="10" form="formulario" required><?php print($cuerpo);?></textarea>
            </div>
            <input type="submit" name="enviar" value="Confirmar">
        </form>

        <?php require "../footer.php" ?>
    </body>
</html>